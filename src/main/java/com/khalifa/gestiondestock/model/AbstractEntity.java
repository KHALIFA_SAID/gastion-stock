package com.khalifa.gestiondestock.model;

import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author KHALIFA SAID - ANDROMEDA - NOVHADRIM
 * @create 12/03/2021 23:17
 */
@Data
@MappedSuperclass
public class AbstractEntity implements Serializable {
    @Id
    private Integer id;
    @CreatedDate
    private Date creationDate;
    @LastModifiedDate
    private Date lastUpdateDate;
}
